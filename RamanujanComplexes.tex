\documentclass[11pt]{article}
\usepackage{fullpage}
\usepackage{rpmacros}
\RequirePackage[colorlinks=true]{hyperref}
\hypersetup{
  linkcolor=[rgb]{0.3,0.3,0.6},
  citecolor=[rgb]{0.2, 0.6, 0.2},
  urlcolor=[rgb]{0.6, 0.2, 0.2}
}
\usepackage{mathpazo}
\usepackage{bbm}
\usepackage{todonotes}
\usepackage{lipsum}
\usepackage{setspace}
\usepackage[T1]{fontenc}
\input{thmmacros}
\onehalfspacing
\title{Ramanujan Expanders and Complexes}%
\author{
Ramprasad Saptharishi\\
Tel Aviv University\\
\texttt{ramprasad@cmi.ac.in}
}
\begin{document}
\maketitle
%\onehalfspace
\begin{abstract}
  This is just an exposition of what I understand of Ramanujan Graphs and Ramanujan Complexes from Tali Kaufman's lectures. 

  The notes are meant for giving an exposition for people who aren't too familiar with these ideas (like me) and are meant to be as elementary as possible. Whenever possible, this exposition will prefer intuition over brevity, even if it is at the cost of taking 3 pages to describe something that can be stated concisely in a few lines (evident from the fact that this is a 10+ page exposition for something that is typically described in no more than 2 or 3 pages by experts).
But if you are the sorts who is comfortable with statements like ``consider a co-compact torsion free subgroup of $\mathrm{PGL}_n(\Q_p)/\mathrm{PGL}_n(\Z_p)$'', then you are looking at the wrong sort of exposition and you can just read the paper instead.
\end{abstract}

\newcommand{\GL}{\operatorname{GL}}
\newcommand{\RPNote}[1]{\textcolor{Red}{\guillemotleft RP: #1\guillemotright}}


\section{Constructing good expanders}

The goal of this exposition is to understand explicit constructions of $d$-regular graphs (and soon to be generalized to complexes) that are \emph{very good} spectral expanders. Recall that a graph $G$ is said to be a $\lambda$-spectral expander if the second largest eigenvalue (in absolute value) of the adjacency matrix $A_G$, denoted by $\lambda_2(G)$ is at most $\lambda$. 

The smaller then $\lambda$, the better is the expansion properties of this graph. How small can $\lambda$ be? 

\begin{theorem}[Alon-Boppana (see \cite{Alon86})]
Any $d$-regular graph $G$ satisfies 
\[
\lambda_2(G) \spaced{\geq} 2\sqrt{d-1}\; (1 - o(1)).
\]
\end{theorem}

Can we find families of bounded degree graphs that achieve $\lambda_2(G) \approx 2\sqrt{d}$? This lecture will outline the construction of Lubotzky, Phillips and Sarnak \cite{LPS88} of an explicit family of bounded degree graphs that match the above lower bound. This construction is quite involved and this note hopefully give a reasonable sense of the construction. 

\subsection{The TL;DR Version}

The main idea of LPS is to start with the best possible expander of degree $d$ --- the infinite tree of degree $d$.
Clearly, one cannot hope for better expansion properties than in the infinite tree, and it is indeed true that the second largest eigenvalue\footnote{Yes, one can define eigenvalues for infinite graphs etc.
but let's not fret about this now.}
of the infinite tree is indeed $2\sqrt{d-1}$.
The task then reduces to somehow \emph{folding} this tree on to itself with just finitely many vertices, that still remains $d$-regular and more or less maintains the spectrum.\\

\noindent
How does one perform such a \emph{folding}? This is where I think the following insight is very useful:

\begin{quote}
Whenever possible, if you want to study a certain set $S$ (that is typically unstructured), find a group $G$ that acts transitively on the set $S$ and study that group instead. 
\end{quote}

This miraculously lets one induce some structure on the set $S$ that can be exploited.
Indeed, often mathematicians then use \emph{representation theory} of the group $G$ to perform magical operations on the set $S$.
LPS also do this and perform the miraculous folding that retains the degree of the graph and the spectrum! To be honest, I don't know the details of how exactly this is done, but this as a general philosophy is important to keep in mind. \\

There are numerous clever ideas used in the LPS construction.
Firstly, the infinite $d$-regular tree will be constructed in a really counter-intuitive way, in which it would not even be clear at first that the graph is connected let alone a $d$-regular tree but we'll see that it indeed is a $d$-regular tree.
The point of this counter-intuitive construction is that we can then find a nice group $G$ that acts on the vertices of the tree.

Once we have a nice group $G$ acting on the vertices, LPS then use representation theory on $G$ to find ways to perform the folding. But this part is beyond the scope of this write-up. \\

OK, let's get started. The whole construction works over what is called a \emph{local field} and the $p$-adic field described below what we shall use in this note. 

\section{The $p$-adic field}

The Ramanujan graphs and complexes use the $p$-adic field in the background. 
Fix a prime $p$. The elements of the \emph{$p$-adic integers}, denoted by $\Z_p$, are infinite sequences $\insquare{a_0,a_1,\ldots}$ where each coordinate $a_i \in \set{0,\cdots, p-1}$. In other words, every element of $\Z_p$ is a function $a: \N \rightarrow \F_p$. 

Intuitively, this infinite sequences should be thought of as a base-$p$ representation of the number, compatible with 
\[
a \bmod p^{i+1} \spaced{=} a_0 + a_1p + a_2 p^2 + \cdots + a_i p^i,
\]
for every $i$. 

For example, the number $17$ in $\Z_3$ would be expressed as $[2,2,1,0,0,0,0,\ldots]$ expected. But the number $-1$ would be expressed as $[(p-1), (p-1),(p-1),\ldots]$ in $\Z_p$, so as to be consistent with $-1 \bmod p^i = p^i -1$. \\

Whenever we have a ring, it is natural to add inverses to every element in it to make it a field $\Q_p$. These are again infinite sequences, but a \emph{finite number of negative indices}. Formally, each element $r \in \Q_p$ is described by a map $r: [-m, -m+1,\ldots, 0,1,2,\ldots] \rightarrow \F_p$ for some finite $m$. That is, there are finitely many negative indices but infinitely many positive indices. \\

OK, all this is a mouthful and it suffices to say that $\Z_p$ is a bizarre ring and $\Q_p$ is a bizarre field. But the following are most important points to keep in mind (and these are all that we would need). 

\begin{itemize}
\item $\Z_p$ is a ring, and $\Q_p$ is a field. More importantly, they are characteristic zero objects, namely adding an element to itself many times does not make it zero (unlike fields like $\F_p$ or $\F_{p^r}$). 

\item $\Z_p$ has familiar integers $\Z$ as a sub-ring and $\Q_p$ has the rationals $\Q$ as a sub-field. Thus, the multiplication and addition operations in $\Q_p$ extend the usual multiplication and addition operations in $\Q$. 

\item For every $a \in \Z_p$ such that $a_0 \neq 0$, there exists a $b\in \Z_p$ such that $a\cdot b = 1$. In other words, all $p$-adic integers that have a non-zero first coordinate has an inverse within the integers. 

Thus, the only non-invertible elements of $\Z_p$ have multiples of $p$. This is in stark contrast with the familiar ring of integers $\Z$, where the only invertible elements (in $\Z$) are $1$ and $-1$. 

\item For every $r \in \Q_p$, there is a large enough integer $N$ such that $p^N \cdot r \in \Z_p$. 

\item If $\phi$ is a map on $\Z_p$ defined so that $\phi(a) = a_0$, then this is a homomorphism from $\Z_p$ to $\F_p$. That is, $\phi(a+b) = \phi(a) + \phi(b)$ and $\phi(ab) = \phi(a) \cdot \phi(b)$. 
\end{itemize}

We shall use $\GL_d(\Q_p)$ to refer to the set of invertible  $d\times d$ matrices with entries from $\Q_p$, and $\GL_d(\Z_p)$ to refer to the set of $d\times d$ matrices with entries from $\Z_p$ that have an inverse with $\Z_p$ entries as well. But for what sorts of matrices $M$ with $\Z_p$ entries have an inverse with integers entries as well? The following claim exactly characterizes all such $M$ and we leave the proof as an exercise. 

\begin{claim}
Let $M \in \Z_p^{d\times d}$ be a $d\times d$ matrix with entries in $\Z_p$. Then, $M$ has an inverse in $\Z_p^{d\times d}$ if and only if $\det(M)$ is not divisible by $p$. 
\end{claim}


These are the basic properties of $\Z_p$ and $\Q_p$ we will need in this notes. We now proceed to describe the Ramanujan Graph. The first step in that is the Bruhat-Tits tree construction. 

\section{The Bruhat-Tits Tree}\label{sec:BT-tree}

We now begin building the regular infinite tree and this is a construction by Fran\c{c}ois Bruhat and Jacques Tits \cite{BT1972}. 
We first need to understand lattices over $\Z_p$.

\subsection{Lattices}

\begin{definition}[Lattices over $\Z_p$] A lattice $L$ generated by $\vecv_1,\ldots, \vecv_d \in \Q_p^d$ is defined as the set of points
\[
L\spaced{=} \setdef{a_1 \vecv_1 + \cdots + a_d \vecv_d}{a_i \in \Z_p\text{ for all $i\in [d]$}},
\]
the set of all $\Z_p$ linear combinations of $\vecv_1,\cdots, \vecv_d$. The dimension of this lattice, denoted by $\dim(L)$, is equal to the rank of $\set{\vecv_1,\ldots, \vecv_d}$. 
\end{definition}

We will only be dealing with lattices in $\Q_p^d$ of full dimension, that is $\dim(L) = d$. This means that the generating set forms a basis for $\Q_p^d$. 

Of course, there are many bases $\set{\vecv_1,\cdots, \vecv_d}$ that may generate the same lattice, for example $\set{\vecv_1+\vecv_2, \vecv_2,\cdots, \vecv_d}$.

\begin{observation}\label{obs:GLZ-within-lattice}
Let $L$ be generated by $\set{\vecv_1,\ldots, \vecv_d}$  and $L'$ be the lattice generated by $\set{\vecu_1,\ldots, \vecu_d}$. If there is a matrix a $M \in \Z_p^{d\times d}$ such that 
\[
\insquare{\begin{array}{ccc}
\vecv_1 & \cdots & \vecv_d
          \end{array}
        } \; M \spaced{=}\insquare{\begin{array}{ccc}
\vecu_1 & \cdots & \vecu_d
                                   \end{array}
                                 },
\]
then $L' \subseteq L$. Furthermore if $M \in \GL_d(\Z_p)$ ,that is $M$ has an inverse in $\Z_p^{d\times d}$ as well, then $L = L'$. 
\end{observation}

Thus, as long as we use elements of $\GL_d(\Q_p)$ with integer entries, this stays within the lattice. 
Of course, we can move from one lattice to another (of the same dimension) by multiplying with an element of $\GL_d(\Q_p)$. But using the fact that every $r \in \Q_p$ has some $N>0$ such that $p^N r \in \Z_p$, we get the following corollary. 


\begin{corollary}\label{cor:pNL-sublattice}
For any pair of lattices $L$ and $L'$ over $\Z_p$ of the same dimension, there exists an $N > 0$ such that 
\[
p^N L' \spaced{\subseteq} L.
\]
In particular, for every lattice $L'$, there is an $N > 0$ such that $p^N L'$ is a sub-lattice of the standard lattice $L_0 = \Z_p^d$. 
\end{corollary}
\begin{proof}
Say $L$ is generated by $\set{\vecv_1,\ldots, \vecv_d}$ and $L'$ is generated by $\set{\vecu_1,\ldots, \vecu_d}$. Since these are both lattices of the same dimension, there is a matrix $M \in \GL_d(\Q_p)$ such that 
\[
\insquare{\begin{array}{ccc}
\vecv_1 & \cdots & \vecv_d
          \end{array}
        } \; M \spaced{=}\insquare{\begin{array}{ccc}
\vecu_1 & \cdots & \vecu_d
                                   \end{array}
                                 }.
\]
But we can always find some $N$ large enough so that $M':= p^N M \in \Z_p^{d\times d}$. Therefore,
\[
\insquare{\begin{array}{ccc}
\vecv_1 & \cdots & \vecv_d
          \end{array}
        } \; M' \spaced{=}p^N \insquare{\begin{array}{ccc}
\vecu_1 & \cdots & \vecu_d
                                   \end{array}
                                 }.
\]
By \autoref{obs:GLZ-within-lattice}, this implies that $p^N L' \subseteq L$. 
\end{proof}


We are now ready to describe the vertices of the Bruhat-Tits tree. 

\subsection*{Vertices of the Bruhat-Tits Construction}

\newcommand{\BT}{\mathrm{BT}}

We shall first describe $\BT(1)$, which yields a $1$-dimensional simplicial complex a.k.a graphs. These would involve $2$-dimensional lattices over $\Z_p$. 

We shall say use an equivalence relation $\sim$ between lattices and say $L_1 \sim L_2$ if there exists an $0\neq \alpha \in \Q_p$ such that $L_1 = \alpha L_2$. That is, we will identify lattices that are \emph{scalings} of one another and denote by $[L_1]$ the equivalence class that $L_1$ belongs to. \\

The vertices of $\BT(1)$ are these equivalence classes $[L_i]$. 
\subsection*{Edges of the Bruhat-Tits Construction}

We first describe the edge relations formally and we will then understand them in a more hands-on way. 

\begin{quote}
The graph $\BT(1)$ has an edge between two vertices $[L_1]$ and $[L_2]$ if there exists representatives $L_1' \in [L_1]$ and $L_2' \in [L_2]$ such that 
\[
p L_1' \;\subsetneq\; L_2' \;\subsetneq\; L_1'.
\]
\end{quote}

What does this mean? Specifically, say we have a vertex $[L_1]$ that is the equivalence class of the lattice generated by $\set{\vecv_1,\vecv_2}$. What vertices $[L_2]$ are its neighbours? 
\begin{eqnarray*}
L_1 & = & \vecv_1 \cdot [\ast,\ast,\ast,\ldots] \;+\; \vecv_2\cdot [\ast,\ast,\ast,\ldots]\\
p \cdot L_1 & = & \vecv_1 \cdot [0,\ast,\ast,\ldots] \;+\; \vecv_2\cdot [0,\ast,\ast,\ldots]
\end{eqnarray*}
So really, if $L_2$ is to be sandwiched between $pL_1$ and $L_1$, the only freedom we have is in the first coordinate of the two directions. For example, $L_2$ could contain points of the form
\[
L_2  \quad\supseteq\quad  \set{\vecv_1 \cdot [a,\ast,\ast,\ldots] \;+\; \vecv_2\cdot [b,\ast,\ast,\ldots]},
\]
but since $L_2$ should be a lattice, this would also force
\[
L_2  \quad\supseteq\quad  \setdef{\vecv_1 \cdot [a\lambda,\ast,\ast,\ldots] \;+\; \vecv_2\cdot [b\lambda,\ast,\ast,\ldots]}{\lambda\in\F_p},
\]
that is, the first coordinates of the two directions form a proper subspace in $\F_p^2$, and hence a line. There are totally $p+1$ lines in $\F_p^2$ that are namely in directions $\setdef{(1,\alpha)}{\alpha\in \F_p} \union \set{(0,1)}$ and each such direction would yield one neighbour of $L_1$. We formalize this as a lemma. 

\begin{lemmawp}\label{lem:BT1-neighs}
Let $L$ be a lattice generated by $\set{\vecv_1,\vecv_2}$. For each linear subspace in $\F_p^2$, define the following lattices:
\begin{eqnarray*}
\text{For each $a\in \F_p$, define}\quad L_{(1,a)} & := & \setdef{\vecv_1 \cdot [\lambda,\ast,\ast,\ldots] \;+\; \vecv_2\cdot [a\lambda,\ast,\ast,\ldots]}{\lambda\in\F_p}\\
        &  = & (\vecv_1 + a\vecv_2)\cdot \Z_p \;+\; (p\vecv_2) \cdot \Z_p\\
        &  = & \insquare{\begin{array}{cc} \vecv_1 & \vecv_2\end{array}}\insquare{\begin{array}{cc} 1 & \\ a & p \end{array}}\\
                                                  & & \\
\text{and}\quad L_{(0,1)} & := & \setdef{\vecv_1 \cdot [0,\ast,\ast,\ldots] \;+\; \vecv_2\cdot [\lambda,\ast,\ast,\ldots]}{\lambda\in\F_p}\\
        &  = & (p\vecv_1)\cdot \Z_p \;+\; \vecv_2 \cdot \Z_p\\
        &  = & \insquare{\begin{array}{cc} \vecv_1 & \vecv_2\end{array}}\insquare{\begin{array}{cc} p & \\  & 1 \end{array}}.
\end{eqnarray*}
The equivalence classes of these lattices are precisely the $(p+1)$ neighbours of $[L]$ in $\BT(1)$. 
\end{lemmawp}

\noindent 
Great!
Now we know exactly what the vertices and the edges are.
What we would like to show now is that this graph is connected, and this graph is actually an infinite tree of degree $(p+1)$.
But we shall defer the proofs of these two facts to the appendix and instead proceed to generalizing the above definition to complexes of higher dimension. 

This generalization of Ramanujan Graphs to complexes is by Lubotzky, Samuels and Vishne~\cite{LSV05}. 

\section{The Bruhat-Tits Building in Higher Dimensions}

\newcommand{\X}{\mathcal{X}}
\renewcommand{\P}{\mathbb{P}}

A graph $G = (V,E)$ is made up of vertices $V$ and edges $E$ between them.
We can think of the vertices as the $0$-dimensions \emph{cells} and edges are $1$-dimensional \emph{cells}, we have the property that for every edge $(u,v) \in E$, the end points $u,v\in V$.

A natural generalization of this to vertices, edges, triangles, etc. is a \emph{simplicial complex}. To us, we simplicial complexes of dimension $d$ is a collection of subsets $\X = (\X(0),\X(1),\ldots, \X(d-1))$, where $\X(i-1)$ refers to the \emph{cells} in $\X$ of \emph{size} $i$, that satisfies the downward-closed property, i.e. if $A \in \X(i-1)$ of \emph{size} $i$ and $B \subset A$ of \emph{size} $j < i$ then $B \in \X(j-1)$. 

\medskip

So for example, we could have $X$ as a higher dimensional analogue of graphs with vertices (elements of $\X(0)$), edges (elements of $\X(1)$) and triangles (elements of $\X(2)$). An excellent example of a higher dimensional simplicial complex is a  \emph{spherical building}. 

\subsubsection*{The Spherical Building}

\begin{definition}[Spherical Building of dimension over $\F^d$]
The \emph{Spherical Building}  over a vector space $\F^d$, denoted by $\P^{d-2}(\F)$ is a simplicial complex defined as follows:
\begin{itemize}
\itemsep 0pt
\item the \emph{vertices}, or elements of $\X(0)$ are \emph{proper subspaces} of $\F^{d}$,
\item $(V_1,V_2,\ldots, V_r) \in \X(r-1)$ if there exists a re-ordering of the $r$ subspaces satisfying
\[
 (0) \subsetneq  V_{i_1} \subsetneq V_{i_2} \subsetneq \cdots \subsetneq V_{i_r} \subsetneq \F^{d}.
\]
Such a chain of inclusions is also called a \emph{flag}. 
\end{itemize}
Since we can have a maximum of $d-1$ proper inclusions of non-trivial subspaces of $\F^d$, the simplicial complex $\P^{d-2}$ has dimension $(d-2)$. 
\end{definition}

Thus in particular, if $d = 3$, we get $\P^{d-2}$ to be a $1$-dimensional simplicial complex a.k.a. graph. In $\P^1$, vertices are either lines or planes in $\F^3$ and you put an edge between a line $\ell$ and a plane $P$ if $\ell \subset P$. This is the graph used in many of the \emph{line-plane} tests in PCP literature. 

\medskip

Let us understand a bit about these spherical buildings over say the field $\F_p$. The number of vertices in $\P^{d-2}$ is the number of proper  subspaces of $\F^{d-1}$. A very useful notation to count subspaces of $\F_p^d$ are these \emph{gaussian binomial coefficients} (just a name!):
\newcommand{\gbinom}[2]{\insquare{\begin{array}{c} #1 \\ #2\end{array}}}
\begin{eqnarray*}
\text{For all $0\leq k \leq d$,}& &\\
 \gbinom{d}{k}_p &:=& \frac{(p^d -1)(p^d - p)\cdots (p^d - p^{k-1})}{(p^k - 1)(p^k - p) \cdots (p^k - p^{k-1})} \;\approx\; p^{k(d-k)}\\
& & \text{the number of $k$-dimensional subspaces in $\F_p^d$}
\end{eqnarray*}

Thus, the number of vertices of $\P^{d-2}$ is
\[
\sum_{k=1}^{d-1} \gbinom{d}{k}_p = p^{O(d^2)}.
\]
In fact, for any $d \geq 3$, the underlying graph of $\P^{d-2}$ (just taking $\X(0)$ and $\X(1)$) is an excellent expander.
The only issue here is that its degree is also $D = p^{O(d^2)}$ which is comparable to the number of vertices.
But of course, if we fix $p$ and $d$, the degree is a constant and so is the number of vertices.

What we shall now do is construct a simplicial complex where the number of vertices is growing but somehow ensure that \emph{locally} it always looks like $\P^{d-2}$.
Once again, we shall construct an \emph{infinite} simplicial complex that is a ``tree'' and this is then folded using similar tricks to obtain the finite simplicial complex called the Ramanujan Complex.

\subsection{The Bruhat-Tits Building}

The Bruhat-Tits building is defined very analogously to \autoref{sec:BT-tree}. This shall once again involve the $p$-adic field $\Q_p$ and the $p$-adic integers $\Z_p$ and lattices over them. 

\medskip
\noindent
\textbf{Vertices of $\BT(d-1)$: } The vertices of the Bruhat-Tits building of dimension $(d-1)$ will be equivalence classes $[L]$ of $d$-dimensional lattices in  $\Q_p^d$ where $L_1 \sim L_2$ if there is some $0\neq \alpha \in \Q_p$ such that $L_1 = \alpha L_2$. 

\medskip
\noindent
\textbf{Edges of $\BT(d-1)$:} Two equivalence classes $[L_1]$ and $[L_2]$ are connected by an edge if there exists representatives $L_1' \in [L_1]$ and $L_2' \in [L_2]$ such that
\[
p L_1' \;\subsetneq\; L_2' \;\subsetneq\; L_1'.
\]

\medskip
\noindent
\textbf{Triangles of $\BT(d-1)$:} Two equivalence classes $[L_1]$, $[L_2]$, $[L_3]$ are connected by a triangle if there exists representatives $L_1' \in [L_1]$, $L_2' \in [L_2]$ and $L_3' \in [L_3]$ such that
\[
p L_1' \;\subsetneq\; L_3' \;\subsetneq\; L_2' \;\subsetneq\; L_1'.
\]

\medskip
And in general:

\medskip
\noindent
\textbf{$i$-cells in  $\BT(d-1)$:} Equivalence classes $[L_1],[L_2],\ldots,[L_i]$ are connected by a $i$-cell if there exists representatives $L_1' \in [L_1],\ldots, L_i' \in [L_i]$ such that
\[
p L_1' \;\subsetneq\; L_i' \;\subsetneq\; \cdots \;\subsetneq\; L_2' \;\subsetneq\; L_1'.
\]

\medskip

Once again, because the cells are defined by some sandwiching between $pL_1'$ and $L_1'$, just like earlier, the only freedom we have is in the first coordinate. 

\begin{lemma}
Fix a lattice $L$ generated by $\set{\vecv_1,\ldots, \vecv_d}$, i.e
\[
L \spaced{=} \vecv_1 \cdot [\ast,\ast,\ast,\ldots] \;+\; \cdots \;+\; \vecv_d \cdot [\ast,\ast,\ast,\ldots]. 
\]
For any proper subspace $W \in \F_p^d$, define the lattice $L_W$ as
\[
L_W \spaced{:=} \setdef{\vecv_1 \cdot [a_1,\ast,\ast,\ldots] \;+\; \cdots \;+\; \vecv_d \cdot [a_d,\ast,\ast,\ldots]}{(a_1,\cdots, a_d) \in W}.
\]
Then, the set of $i$-cells that $[L]$ is a part of in $\BT(d-1)$ is precisely $([L],[L_{W_1}],\cdots, [L_{W_{i-1}}])$ that satisfies
\[
(0) \subsetneq L_{W_1} \subsetneq \cdots \subsetneq L_{W_{i-1}} \subsetneq \F_p^d. 
\]
\end{lemma}

In other words, the $i$-cells are exactly the \emph{flags} in the spherical building! In fact, this goes deeper. 

\begin{definition}[Link of a cell in a complex]
The \emph{link} $\X_\sigma$ of any cell $\sigma \in \X$ in the complex $\X$ is defined as
\[
\X_\sigma \spaced{:=} \setdef{\tau \setminus \sigma}{\tau \in \X\,\text{and}\,\tau \supset \sigma},
\]
the restriction of $\X$ to only the cells containing of $\sigma$ and removing $\sigma$ from each of them. 
\end{definition}


\begin{corollary}
The \emph{link} $\X_\sigma$ of any cell $\sigma \in \X(i)$ in the complex $\BT(d-1)$ is precisely the spherical building $\P^{d-i-1}$. 
\end{corollary}

As mentioned earlier, the spherical buildings are excellent expanders (at least the underlying graphs of them). The Bruhat-Tits building has the remarkable property that \emph{every} link is a spherical building!\\

Once again, it is not hard to show that the underlying graph ($\X(0)$ and $\X(1)$) is connected and is infact a $D$-ary tree where
\[
D\spaced{=} \sum_{k=1}^{d-1} \gbinom{d}{k}_p.
\]

\section{Groups acting on Bruhat-Tits buildings}

I do not know too many details about this part but here are some thoughts about this.
So far, we have constructed infinite objects that locally have the desirable properties that we would like from our complex.
As mentioned earlier, the finite step is to somehow \emph{fold} this infinite object to one over finitely many vertices but still preserving the important properties like the \emph{spectrum} and \emph{bounded-degree-ness}. \\

The vertices of the Bruhat-Tits building $\BT(d-1)$ are equivalence classes of lattices in $\Q_p^d$. A natural group that acts on lattices is $\GL_d(\Q_p)$, the class of $d\times d$ invertible matrices. Is the action well-defined on equivalence classes as well? Indeed yes. Let $L_1$ be a lattice and $M \in \GL_d(\Q_p)$ be a matrix so that $L_1 \cdot M = L_2$. Then for any other $L_1' = \alpha L_1 \in [L_1]$, we have
\[
L_1' \cdot M \;=\; \alpha L_1 \cdot M \;=\; \alpha L_2 \in [L_2]. 
\]
Let us perform the same equivalence operation on the group $\GL_d(\Q_p)$. 

\newcommand{\PGL}{\operatorname{PGL}}
\begin{definition}
Define the group $\PGL_d(\Q_p)$ as equivalence classes of invertible matrices in $\Q_p^{d\times d}$ where $M_1 \sim M_2$ if $M_1 = \alpha M_2$ for some $0\neq \alpha \in \Q_p$. 
\end{definition}
This operation of identifying elements with its scalings is rather standard in mathematics and the $\mathrm{P}$ stands for \emph{projective}. \\



Thus, we get the following observation. 
\begin{observation}
The group $\PGL_d(\Q_p)$ acts transitively on the set of vertices of $\BT(d-1)$. 
\end{observation}

One cool way to associate a group structure on an arbitrary set is to take a transitive group $G$ acting on it and \emph{quotient it by the stabilizer}. 

\begin{definition}[Stabilizer of a group action]
Suppose a group $G$ acts on a set $S$ and let $x_0 \in S$. The stabilizer of $x_0$, denoted by $\mathrm{stab}_G(x_0)$ is defined as
\[
\mathrm{stab}_G(x_0) \spaced{=}\setdef{g\in G}{g(x_0) = x_0},
\]
the subgroup of $G$ that does not move $x_0$. 
\end{definition}

\begin{lemmawp}
If a group $G$ acts transitively on a set $S$, then the elements of the set $S$ can be identified by the quotient
\[
\frac{G}{\mathrm{stab}_G(x_0)}
\]
for any $x_0 \in G$. 
\end{lemmawp}

Great! We already have a transitive action on the vertices of $\BT(d-1)$ which is $G = \GL_d(\Q_p)$. What are the elements that fix say the equivalence class of the standard lattice $L_0 = \Z_p^d$? These are precisely matrices $M$ that can be written as $\alpha \cdot M'$ where $M' \in \GL_d(\Z_p)$. Thus, 
\[
\mathrm{stab}_G([L_0]) \spaced{=} \PGL_d(\Z_p)
\]
defined analogously. Thus, we can identify the vertices of $\BT(d-1)$ with the group quotient
\[
\frac{\PGL_d(\Q_p)}{\PGL_d(\Z_p)}.
\]

Once we have such an identification, one can (apparently) use \emph{representation theory} of this group quotient to find the right foldings.
As confessed earlier, I do not know more about this. But yes, this involves a co-compact torsion-free subgroup of $\frac{\GL_d(\Q_p)}{\GL_d(\Z_p)}$, whatever that means!
But hopefully this gives the reader something. 

\section{Why the Bruhat-Tits tree is indeed a tree}

This section would give all the necessary ideas required to prove that the Bruhat-Tits constructions yield a $(p+1)$-regular tree. The reader is encouraged to fill in the missing steps of the proof outlined here. \\

Let us focus on $\BT(1)$ defined in \autoref{sec:BT-tree} but whatever is discussed here would also apply to the underlying graph of $\BT(d-1)$ in general. In order to show that $\BT(1)$ is a $(p+1)$-regular tree, we need to show three things:
\begin{itemize}
\itemsep 0pt
\item show that it is $(p+1)$-regular (we have already done this part),
\item show that every vertex $[L]$ has a path to say the standard lattice $[L_0] = \Z_p^2$,
\item show that every vertex $[L]$ has a \emph{unique} path to $[L_0]$. 
\end{itemize}

\noindent 
Before we get to this, we need to be able to answer the following question:
\begin{quote}
If you are given two lattices $L_1$ and $L_2$ by a basis for each, can you check if $L_1$ and $L_2$ are the same lattice? 
\end{quote}

The standard way to do this is to convert the given basis to some \emph{normal form} such that two lattices $L_1$ and $L_2$ are identical if and only if their normal forms are identical. This is precisely what the \emph{Hermite Normal Form} achieves. 

\subsection{Hermite Normal Form}

We first describe the Hermite Normal form for lattices over the rationals $\Q$ with integers  being the familiar integers $\Z$, and then list the definition to $\Q_p$ and $\Z_p$. 

\begin{definition}[Hermite Normal Form, for $\Z$-lattices]
A $d\times d$ matrix non-singular matrix $M$ with integers is said to be in \emph{Hermite Normal Form (HNF)} if it satisfies the following properties:
\begin{itemize}\itemsep 0pt
\item $M$ is lower-triangular, that is $M_{i,j} = 0$ whenever $j > i$,
\item every entry of $M$ is non-negative,
\item in each row $i$, the largest entry is $M_{ii}$, the diagonal entry.\qedhere
\end{itemize}
\end{definition}

\begin{lemmawp}[Uniqueness of Hermite Normal Form]
For any $d\times d$ non-singular matrix $M$ with integer entries, there is a unique matrix $H$ in Hermite Normal Form such that $M\cdot U = H$ for some $U \in \Z^{d\times d}$ that has an inverse in $\Z^{d\times d}$. 
\end{lemmawp}

Thus, to check if two lattices are equal we can just compute the HNF of the basis and check if they are identical.
The proof of the above lemma quite easy. The interested reader is encouraged to prove it by themselves. \\

There is a natural generalization for $\Z_p$ lattices as well but we would need the following definition. For some $a = [a_0,a_1,\ldots] \in \Z_p$, we shall say that $a \prec p^r$ if $a_i = 0$ for every $i \geq r$. That is, $a \in \Z \subset \Z_p$ and $a < p^r$ as an integer. 

\begin{definition}[Hermite Normal Form, for $\Z_p$-lattices]
A $d\times d$ matrix non-singular matrix $M$ with $\Z_p$ entries is said to be in \emph{Hermite Normal Form (HNF)} if it satisfies the following properties:
\begin{itemize}\itemsep 0pt
\item $M$ is lower-triangular, that is $M_{i,j} = 0$ whenever $j > i$,
\item each diagonal entry of $M$ is a power of $p$,
\item in each row $i$, if the diagonal entry $M_{ii} = p^r$ for some $r$, then each  $M_{ij} \prec p^r$ for all $j < i$.\qedhere
\end{itemize}
\end{definition}

\begin{lemmawp}[Uniqueness of Hermite Normal Form, over $\Z_p$]
For any $d\times d$ non-singular matrix $M$ with $\Z_p$ entries, there is a unique matrix $H$ in Hermite Normal Form such that $M\cdot U = H$ for some $U \in \GL_d(Z_p)$.
\end{lemmawp}

We are now ready to show that the Bruhat-Tits tree $\BT(1)$ is indeed a tree. 

\begin{theorem}
The $\BT(1)$ is a $(p+1)$-ary infinite tree. 
\end{theorem}
\begin{proof}
It suffices to show that any vertex $[L]$ has a unique path to the vertex corresponding to the standard basis $[L_0]$. Consider the HNF of the lattice $L$ and suppose it is
\[
H \spaced{=} \insquare{\begin{array}{cc}
p^r & \\
a & p^s 
\end{array}}
\]
with $a = a_0 + a_1p + \cdots a_{s-1}p^{s-1}$. Then observe that
\[
H \spaced{=}\insquare{\begin{array}{cc}
p & \\
 & 1 
\end{array}}^r \insquare{\begin{array}{cc}
1 & \\
a_0 & p 
\end{array}}\insquare{\begin{array}{cc}
1 & \\
a_1 & p 
\end{array}}\cdots\insquare{\begin{array}{cc}
1 & \\
a_{s-1} & p 
\end{array}}.
\]
And note that these are precisely the edges in $\BT(1)$. Furthermore, this is also unique in the sense that if $a < p^s$ then
\[
\insquare{\begin{array}{cc}
1 & \\
a & p^s 
\end{array}} \spaced{\stackrel{\text{uniquely}}{=}}\insquare{\begin{array}{cc}
1 & \\
a_0 & p 
\end{array}}\insquare{\begin{array}{cc}
1 & \\
a_1 & p 
\end{array}}\cdots\insquare{\begin{array}{cc}
1 & \\
a_{s-1} & p 
\end{array}}.
\]
But why are we not considering taking an edge such as $\insquare{\begin{array}{cc}1 &\\a & p\end{array}}$ and then $\insquare{\begin{array}{cc}p &\\ & 1\end{array}}$? The reason is that
\[
\insquare{\begin{array}{cc}1 &\\a_i & p\end{array}}\insquare{\begin{array}{cc}p &\\ & 1\end{array}} \spaced{=} \insquare{\begin{array}{cc}p &\\a_ip & p\end{array}} \stackrel{\mathrm{HNF}}{\equiv} \insquare{\begin{array}{cc}p &\\ & p\end{array}}.
\]
In other words if you start with a vertex, take the edge corresponding to the subspace $(1,a)\F_p$ in $\BT(1)$ and then take the edge corresponding to the subspace $(0,1)\F_p$, you end up with the lattice that is just a scaled version of the original lattice. In other words, you returned to the same vertex in $\BT(1)$. 

Thus, every vertex $[L]$ has a unique path to $[L_0]$ in $\BT(1)$ and hence we have that $\BT(1)$ is indeed a $(p+1)$-regular tree. 
\end{proof}

The same proof essentially works for the graph of $\BT(d-1)$ in general. Try it for $\BT(2)$ and convince yourself that the graph of $\BT(d-1)$ is indeed a $D$-regular tree for
\[
D \spaced{=} \sum_{k=1}^{d-1}\gbinom{d}{k}_p.
\]

\bibliographystyle{customurlbst/alphaurlpp}
\bibliography{references}


\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
