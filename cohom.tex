\documentclass[11pt]{article}
\usepackage{fullpage}
\usepackage{rpmacros}
\RequirePackage[colorlinks=true]{hyperref}
\hypersetup{
  linkcolor=[rgb]{0.3,0.3,0.6},
  citecolor=[rgb]{0.2, 0.6, 0.2},
  urlcolor=[rgb]{0.6, 0.2, 0.2}
}
\usepackage{mathpazo}
\usepackage{bbm}
\usepackage{todonotes}
\usepackage{lipsum}
\usepackage{setspace}
\usepackage[T1]{fontenc}
\usepackage{float}
\input{thmmacros}
\onehalfspacing
\title{Cohomologies for dummies}%
\author{
Ramprasad Saptharishi\\
Tel Aviv University\\
\texttt{ramprasad@cmi.ac.in}
}
\begin{document}
\maketitle
\onehalfspace

\newcommand{\X}{\mathcal{X}}
\newcommand{\Gray}[1]{\textcolor{gray}{#1}}
\newcommand{\dist}{\operatorname{dist}}

The goal of this exposition is to explain some of the basics in cohomology tailored specifically towards understanding simplicial complexes and higher dimensional expanders.
There is surely going to be massive over-simplification.
Many of the terms used here have a broader meaning but throughout this exposition I would focus one just the goal of establishing just enough terminology to understand higher dimensional expanders.

\section{Simplicial Complexes}

In many areas of theoretical computer science, it is useful to model objects we study by associated graphs.
Edges between vertices capture \emph{relations} between various elements of the structure.
A \emph{simplicial complex}, to me, is a generalization of this where relations need not just be between two vertices but maybe even between larger number of them.

\begin{definition}[Simplicial Complex]
A \emph{simplicial complex} $\X$ is a collection of finite subsets of a base set $X$ that is \emph{down-closed}, that is it has the property that if $A \in \X$ and $B \subseteq A$ then $B \in \X$. 

The elements of $\X$ are called \emph{cells} and the base set $X$ is called the \emph{vertices} of $\X$. \\

\noindent 
We shall denote by $\X(i)$ the set of elements of $\X$ of size $(i+1)$ \Gray{(yes... I know... but if you think about it, it makes sense)}:
\[
\X(i) \spaced{:=} \setdef{A \in \X}{|A| = i+1}.
\]
The \emph{dimension of $\X$}, denoted by $\dim(\X)$, is the largest $i$ such that $\X(i) \neq \emptyset$. In other words, $\dim(\X)$ is the size of the largest cell in $\X$ minus one. 
\end{definition}

Recall graphs as a running example. We have vertices $V$ and edges $E$ between them. This is just a $1$-dimensional simplicial complex $\X$ with $\X(0) = V$ and $\X(1) = E$. A $2$-dimensional simplicial complex will have vertices, edges and triangles. And so on. 

\medskip

It is useful to have a picture like the following in mind when thinking about simplicial complexes.

\begin{figure}[H]
\begin{center}
\tikzset{
  every edge/.append style = {draw=gray}
}
\begin{tikzpicture}
\draw[rounded corners, draw=gray!50] (-0.5,3) rectangle (0.5,-3);
\draw[rounded corners, draw=gray!50] (1.5,3) rectangle (2.5,-3);
\draw[rounded corners, draw=gray!50] (3,3) rectangle (5,-3);
\draw[rounded corners, draw=gray!50] (5.5,3) rectangle (7.5,-3);

\node at (0,3.5) {\scriptsize $\X(-1)$};
\node at (2,3.5) {\scriptsize $\X(0)$};
\node at (4,3.5) {\scriptsize $\X(1)$};
\node at (6.5,3.5) {\scriptsize $\X(2)$};

\node (emptyset) at (0,0) {$\emptyset$};

\node (v1) at (2,1.5) {$\set{v_1}$}
edge [<-] (emptyset);
\node (v2) at (2,0.5) {$\set{v_2}$}
edge [<-] (emptyset);
\node at (2,-0.5) {$\vdots$};
\node (vn) at (2,-1.5) {$\set{v_n}$}
edge [<-] (emptyset);

\node (v1v2) at (4,2) {$\set{v_1,v_2}$}
edge [<-] (v1)
edge [<-] (v2);
\node (v2v3) at (4,1) {$\set{v_2,v_3}$}
edge [<-] (v2);
\node (v2vn) at (4,0) {$\set{v_2,v_n}$}
edge [<-] (v2)
edge [<-] (vn);
\node at (4,-1) {$\vdots$};
\node at (4,-2) {$\set{v_{n-1},v_n}$}
edge [<-] (vn);

\node (v1v2v3) at (6.5, 1) {$\set{v_1,v_2,v_3}$}
edge [<-] (v1v2)
edge [<-] (v2v3);
\node at (6.5,0) {$\vdots$};
\node (v2v3vn) at (6.5,-1) {$\set{v_2,v_3,v_n}$}
edge [<-] (v2v3)
edge [<-] (v2vn);

\node at (8,0) {$\cdots$};
\end{tikzpicture}
\caption{Layered representation of simplicial complexes}
\label{fig:sc-layer}
\end{center}
\end{figure}

\textbf{Throughout this discussion, we shall fix our field $\F$ to be $\F_2$} as it has the very useful property that $1 = -1$; this would greatly simplify the exposition here.

\subsection{Co-chains := Functions on layers}


The next object that we should know are called \emph{co-chains}, which are just functions from a layer to $\F_2$. 

\begin{definition}
A \emph{co-chain} on level $i$ is a function $A: \X(i) \rightarrow \F_2$. We shall use $C_i$ to denote the set of all co-chains on level $i$:
\[
C_i \spaced{:=} \set{A:\X(i) \rightarrow \F_2}.
\]
We will often think of a co-chain $A \in C_i$ as a subset of $\X(i)$ (that are ``accepted'' by $A$). 
\end{definition}
\begin{quote}
Q: Why are they called `co-chains'? 

A: I don't know of a satisfactory answer for why they are like chains. But I do know what the 'co-' means; it is to point out that this is \emph{contra-variant}. Let me explain. 

Say you have a map $\Phi: G \rightarrow H$ between two graphs. Now say you want to understand $C_0(G)$, the  functions on the vertices of $G$, and $C_0(H)$, the functions on vertices of $H$. Then, the map $\Phi$ induces a map $\tilde{\Phi}:C_0(H) \rightarrow C_0(G)$ as follows: 
\begin{quote}
  Given an $f \in C_0(H)$, map $g \in C_0(G)$ defined by $g(v) = f(\Phi(v))$. 
\end{quote}
The fact that a map from $G \rightarrow H$ induced a map between $C_0(H) \rightarrow C_0(G)$ is what is referred to as \emph{contra-variant}. That's what the `co-' is for. 
\end{quote}

Another way to interpret a co-chain at level-$i$ as just labeling each element of slice $i$ (in \autoref{fig:sc-layer}) by an element of $\F$. The next thing we would want to do is if we can lift a function on vertices to another function on edges, or vice-versa. 

\subsection{Boundary and co-boundaries}

\begin{definition}[Boundary operator]
The boundary operator, denoted by $\partial$, maps co-chains in $C_i$ to co-chains in $C_{i-1}$ in the following way: if $A: \X(i) \rightarrow \F$ then the function $\partial A: \X(i-1) \rightarrow \F$ is defined by
\[
(\partial A)(\sigma) := \sum_{\substack{\tau \in \X_i\\ \sigma\subset \tau}} A(\sigma)\quad\text{for every $\sigma \in \X(i-1)$}.\qedhere
\]
\Gray{(Generally, there is a subscript on the $\partial$ to denote the layer but I am going to ignore it. There is a bigger chance of me putting the wrong index and confusing the reader than not putting it at all.)}
\end{definition}

For example, if we have a function $A$ on the \emph{edges} of a graph, then the function $\partial A$ is defined on \emph{vertices} by just adding the values on the edges incident on it. 

\medskip

Similarly, one can \emph{lift} functions on vertices to functions on edges. 

\begin{definition}[Co-boundary operator]
The \emph{co-boundary operator}, denoted by $\delta$, maps co-chains in $C_i$ to co-chains in $C_{i+1}$ in the following way: if $A: \X(i) \rightarrow \F$ then the function $\delta A: \X(i+1) \rightarrow \F$ is defined by
\[
(\delta A)(\sigma) := \sum_{\substack{\tau \in \X_i\\ \sigma\supset \tau}} A(\sigma)\quad\text{for every $\sigma \in \X(i+1)$}.\qedhere
\]
\end{definition}
For example, if we have a function $A$ on the \emph{vertices} of a graph, then the function $\delta A$ is defined on \emph{edges} by just adding the values on the end-points. \\

\noindent
I always keep getting confused between which way $\partial$ and $\delta$ operate. Irit Dinur suggested this:
\begin{quote}
Keep \autoref{fig:sc-layer} in mind. The top of $\delta$ curves to the right, so it maps elements of $C_i$ to $C_{i+1}$. And the top of $\partial$ curves to the left, so it maps $C_i$ to $C_{i-1}$. 
\end{quote}

We have already seen that the co-boundary operator $\delta$ ``lifts'' functions from $C_i$ to $C_{i+1}$.
If we think of $i = 0$, we are lifting functions on vertices to functions on edges.
Intuitively, any function on edges that is derived this way is in some sense ``simple'' as it really comes from a level below. In this language, the range of the co-boundary operator is synonymous to ``simple''. These are called \emph{co-boundaries} and denoted by $B_i$.
\[
B_i \spaced{:=} \setdef{\delta (f)}{ f\in C_{i-1}}. 
\]

\noindent 
Almost there! Just one more definition and we can define cohomologies.

\subsection{Co-cycles and cohomologies}

Recall co-boundary operator $\delta$ maps elements of $C_i$ to $C_{i+1}$. There may be functions $f \in C_i$ such that $\delta f$ is the zero function on $\X(i+1)$. These are called \emph{co-cycles}. 

\begin{definition}[Co-cycles]
The co-cycles at level $i$, denoted by $Z_i$, is defined as the kernel of $\delta$ at level $i$. That is,
\[
Z_i \spaced{:=} \setdef{f \in C_i}{\delta f = 0}\qedhere
\]
\end{definition}

\noindent 
Now for the first theorem. 

\begin{theorem}[``$\delta\delta = 0$'']\label{thm:deltadelta}
For any function $f \in C_i$, we have $\delta(\delta(f)) \in C_{i+2}$ is the zero function.
\end{theorem}
\begin{proof}
Important exercise! Don't go beyond this point without working this out! 
\end{proof}

\noindent
Note that $B_i$ and $Z_i$ are spaces of functions over $\F_2$ and are in fact \emph{vector spaces}. The above theorem shows that  $B_i$ is a subspace of $Z_i$. The whole point of cohomologies is to study if $Z_i$ is bigger than $B_i$ or not. 

\begin{definition}[Cohomologies]
The \emph{$i$-th cohomology}, denoted by $H_i$, is defined as the group quotient
\[
H_i \spaced{:=} \frac{Z_i}{B_i}\qedhere
\]
\end{definition}

These are quite a few definitions and they are best understood by taking examples. 

\section{Examples}

Let us just work with graphs for now. A graph $G = (V,E)$ is just a $1$-dimensional  simplicial complex $\X$ with $\X(0) = V$ and $\X(1) = E$ (and $\X(-1) = \set{\emptyset}$). 

\begin{question*}
What is $B_0$, the set of functions on vertices that are co-boundaries?
\end{question*}

Recall that $B_0 = \setdef{\delta(f)}{f\in C_{-1}}$. But what is $C_{-1}$? These are functions on $\X(-1) = \set{\emptyset}$ and there are just two of them -- the function $f_0$ that maps $\emptyset$ to zero, and the function $f_1$ that maps $\emptyset$ to one. 

What is $\delta f_0$? By defintion, $\delta f_0$ on $\set{v}$ is equal to $\sum_{\tau \subset \set{v}} f_0(\tau) = f_0(\emptyset) = 0$. Thus, $\delta f_0$ is just the all zero function on the vertices, and similarly $\delta f_1$ is the all ones function on vertices. 

\medskip 

In other words, the co-boundaries at level-$0$ are the constant functions. 

\begin{question}
What is $B_1$, the set of functions on edges that are co-boundaries?
\end{question}

Recall that $B_0 = \setdef{\delta(f)}{f\in C_0}$ and functions from $V$ to $\F_2$ can just be thought of as choosing a subset of vertices via $S = \setdef{v}{f(v) = 1}$. What is $\delta f$? Fix an edge $(u,v)$. Then by definition, $(\delta f)(\set{u,v}) = f(u) + f(v)$. Since we are working over $\F_2$, this would be one if and only if exactly one of $\set{u,v} \in S$. Hence, the function $(\delta f)$ ``accepts'' only those edges $(u,v)$ such that exactly one of its end-points is in $S$, which is just the \emph{cut-edges} induced by $S$. 

\medskip 

In other words, a subset $A \subseteq E$ of edges is a co-boundary if and only if $A = E(S,\overline{S})$ for some $S\subseteq V$.

\begin{question}
What is $Z_0$, the set of functions $f$ on vertices such that $\delta f = 0$? And what is $H_0$, the $0$-th cohomology?
\end{question}

Recall that $(\delta f)(\set{u,v}) = f(u) + f(v)$. Hence, if $\delta f = 0$, we must have $f(u) = f(v)$ for every edge $(u,v) \in E$. Certainly the constant functions satisfy this property. Is that all? Or are there non-constant functions that also satisfy $f(u) + f(v) = 0$ for every $(u,v)\in E$? 

This answer depends on whether or not the graph $G$ is connected. If the graph has two disconnected components $G_1$ and $G_2$, we could take a function $f$ that is $1$ on all the vertices in $G_1$ and $0$ on all vertices of $G_2$. That would still satisfy the $f(u) + f(v) = 0$ for every $(u,v)\in E$. It is not hard to see that these are precisely all the functions that satisfy this property. 


Therefore, $Z_0$ is just the set of functions that are constant on each connected component of $G$. We therefore get the following observation. 

\begin{lemmawp} For any graph $G$, the dimension of the $0$-th cohomology $H_0$ is exactly the number of connected components of $G$. 
\end{lemmawp}

Now suppose we have a $2$-dimensional simplicial complex $\X = (V,E,T)$ (where $T$ is a set of triangles), we can also try and understand $Z_1$. 

\begin{question}
What is $Z_1$, the set of functions $f$ on edges such that $\delta f = 0$?
\end{question}

If $\delta f = 0$, then for any triangle $\set{u,v,w} \in T$ we must have $f(\set{u,v}) + f(\set{v,w}) + f(\set{u,w}) = 0$. Thus, if we were to interpret $f$ as a subset $A = \setdef{(u,v)}{f(\set{u,v}) = 1}$ of edges, we must have the property that each triangle in $T$ must either include exactly two edges from $A$ or no edges from $A$. 

Note that if $A = E(S,\overline{S})$ for some $S \subseteq V$, then indeed each triangle will include either two or no edges from $A$. But are there other subsets of edges that have this property? Once again, this depends on the underlying structure of the simplicial complex. For now, we shall give an example of a simplicial complex where there are indeed subsets that are in $Z_1$ but are not cuts. 
\begin{figure}[H]
\begin{center}
\begin{tikzpicture}
\node[circle, minimum size=0.1cm,draw=black] (1) at (0,0) {1};
\node[circle, minimum size=0.1cm,draw=black] (2) at (1,2) {2}
edge[draw=blue,thick] (1);
\node[circle, minimum size=0.1cm,draw=black] (3) at (2,0) {3}
edge[draw=blue,thick] (2)
edge (1);
\node[circle, minimum size=0.1cm,draw=black] (4) at (3,2) {4}
edge[draw=blue,thick] (3)
edge (2);
\node[circle, minimum size=0.1cm,draw=black] (5) at (4,0) {5}
edge[draw=blue,thick] (4)
edge (3);
\node[circle, minimum size=0.1cm,draw=black] (1a) at (5,2) {1}
edge[draw=blue,thick] (5)
edge (4);
\node[circle, minimum size=0.1cm,draw=black] (2a) at (6,0) {2}
edge[draw=blue,thick] (1a)
edge (5);
\end{tikzpicture}
\end{center}
\caption{\Mobius{} triangulation -- An example where $Z_1 \neq B_1$}
\end{figure}
\noindent 
Consider the above example where $\X(2) = \set{\set{1,2,3},\set{2,3,4},\set{3,4,5},\set{4,5,1},\set{5,1,2}}$, and $\X(1)$ and $\X(0)$ are just subsets of these triangles. If 
\[
A\spaced{=}\set{\set{1,2},\set{2,3},\set{3,4},\set{4,5},\set{5,1}},
\]
observe that every triangle in $\X(2)$ includes exactly two edges in $A$ and hence $\delta A = 0$. But $A$ is \emph{not} a cut. 

\subsection{Cohomologies as property testing of ``simple'' functions}

Although we defined cohomologies in an abstract setting, it is important to understand the underlying philosophy that it captures. We are interested in a certain class of \emph{simple} objects, which in this case was co-boundaries. What we have are tests that all co-boundaries satisfy. A necessary condition for a function to be a co-boundary is that they must become zero when hit with co-boundary operator again since we know that $\delta \delta = 0$ (by \autoref{thm:deltadelta}). The question is whether this is a sufficient condition as well. The cohomology being trivial is just saying that this is indeed a sufficient condition. 


\section{Connectivity and co-boundary expansion}

Since connectivity of a graph is captured by the $0$-th cohomology being trivial, we shall generalize this notion to complexes and define \emph{homological connectivity} in the following natural way. 

\begin{definition}[Homological Connectivity]
A simplicial complex $\X$ is said to be \emph{homologically connected} if $Z_i = B_i$ for every $i\geq 0$ (or in other words, $H_i$ is trivial for all $i \geq 0$). 
\end{definition}

In other words, a function $f \in C_i$ is a co-boundary \emph{if and only if} it becomes zero when hit with the co-boundary operator again. Thus $\delta f = 0$ is a necessary and sufficient test for $f = \delta g$ for some $g \in C_{i-1}$. \\

\todo[inline]{Write the proof that the \emph{complete simplicial complex} is homologically connected. }

We now want to generalize the notion of \emph{expansion} to higher dimensional simplicial complexes. Let us first revisit the notion of expansion in graphs and try to state things in terms of cohomologies. 

\subsection{Revisiting Cheeger}

Expanders graphs are captured by what is known as the Cheeger constant. 

\begin{definition}[Cheeger Constant and vertex expansion]
For a graph $G$, define the parameter $h(G)$ to be
\[
h(G) \spaced{:=} \min_{\emptyset \neq S \subsetneq V} \frac{\norm{E(S,\overline{S})}}{\norm{\min(|S|,|\overline{S}|)}}
\]
where $\norm{E(S,\overline{S})} = \abs{E(S,\overline{S})}/|E|$ and $\norm{\min(|S|,|\overline{S}|)} = \min(|S|,|\overline{S}|)/|V|$. \\

A graph $G$ is said to be an \emph{$\epsilon$-vertex-expander} if $h(G) \geq \epsilon$. 
\end{definition}

\noindent
Let us try to write this in terms of notation from cohomologies so that we can generalize it to higher dimensional simplicial complexes.
The Cheeger constant is a minimum over sets of vertices that are neither empty nor full.
If we were to think of such a subset $S$ as a function $f_S$ on vertices, then we are essentially excluding the constant functions. Note that the constant functions were precisely $B_0$, the co-boundaries at level $0$. Hence, we can think of the minimum as being over all functions $f_S \in C_0 \setminus B_0$. 

Let us now focus on the numerator. For a set $S \subset V$, can we express $\abs{E(S,\overline{S})}$ in the language of cohomologies? Indeed we can. This is precisely the hamming weight of $\delta S$, that is the number of edge $(u,v)\in E$ such that $(\delta S)(\set{u,v}) = 1$. Therefore, $\norm{E(S,\overline{S})}$ is the normalized weight of $\delta S$ defined as
\[
\norm{\delta S} \spaced{:=} \frac{\abs{\setdef{\sigma \in \X(1)}{(\delta S)(\sigma) = 1}}}{\abs{\X(1)}} \spaced{=} \norm{E(S,\overline{S})}
\]

Now for the denominator. How can $\min(|S|, |\overline{S}|)$ be expressed in the language of cohomologies? This is precisely the distance of the function $f_S$ from $B_0$, the constant functions. Normalizing it again, we have
\begin{eqnarray*}
\dist(f_S,B_1) &:= & \min_{g \in B_1} \abs{\setdef{\sigma \in \X(0)}{f_S(\sigma) = g(\sigma)}}\\
& = & \min(|S|,|\overline{S}|) \\
\norm{\dist(f_S,B_0)} &:= & \frac{\dist(f_S,B_0)}{\abs{\X(0)}}
\end{eqnarray*}
\medskip

Therefore, the expression for $h(G)$ can be rewritten as
\[
h(G) \spaced{=} \min_{f_S \in C_0 \setminus B_0} \frac{\norm{\delta f_S}}{\norm{\dist(f_S,B_0)}}.
\]

In other words, $G$ is an $\epsilon$-vertex-expander if the normalized weight of $\delta f_S$ is \emph{proportionally large} compared to the distance of $f_S$ from $B_0$.
This expression is now in a form that can certainly be lifted to higher dimensional simplicial complexes as well.

\begin{definition}[Co-boundary expanders]
Let $\X$ be a simplicial complex. We shall say that $\X$ is an $\epsilon$-coboundary expander if 
for all $i \geq 0$, we have
\[
\mathcal{E}_i \spaced{:=} \min_{f \in C_i \setminus B_i} \frac{\norm{\delta f}}{\norm{\dist(f,B_i)}} \spaced{\geq} \epsilon. \qedhere
\]
\end{definition}

In the case of connectivity, we said that a function $f$ is a co-boundary if and only if $\delta f = 0$. Co-boundary expansion is a robust version of this statement where we are saying that if $f$ is \emph{far} from being a co-boundary, then $\delta f$ is far from zero. 

For example, consider a $2$-dimensional simplicial complex that is an $\epsilon$-coboundary expander. Then this implies that if we have a subset $A$ of edges such that we need to change $\alpha$-fraction of the total number of edges to transform $A$ to a cut, then there must be an $\epsilon \alpha$-fraction of all triangles in $\X(2)$ that involve and odd number of edges from $A$. \\

What are some explicit simplicial complexes that are co-boundary expanders? Certainly the first complex to try is $\Delta_d$,  the \emph{complete} $d$-dimensional simplicial complex that involves \emph{all} possible subsets of size at most $d+1$ over a vertex set. Indeed, it is known that $\Delta_d$ is a $1$-coboundary  expander. The proof is not hard, apparently but I don't yet know how to prove it. 

\begin{theorem}[Gromov]
For any $d \geq 0$,  $\Delta_d$ is a $1$-coboundary expander.
\end{theorem}

What about other examples? Are there \emph{low-degree} co-boundary expanders? We do not have explicit families of bounded degree co-boundary expanders yet! But we do have some candidates of bounded degree simplicial complexes that are conjectured to be co-boundary expanders. These are the Ramanujan Complexes (which will be dealt with in a different exposition). 


\end{document}




%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
